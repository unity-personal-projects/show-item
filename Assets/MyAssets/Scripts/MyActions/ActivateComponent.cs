﻿using UnityEngine;
using HutongGames.PlayMaker;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.GameObject)]
	[Tooltip("Activate a component")]
	public class ActivateComponent : FsmStateAction
	{

		[RequiredField]
		public FsmGameObject myObject;
		[RequiredField]
		[UIHint(UIHint.ScriptComponent)]
		public FsmString componentName;
		[RequiredField]
		public bool activate = false;


		public override void Reset()
		{
			myObject = null;
			componentName = null;
			activate = false;
		}

		public override void OnEnter()
		{
			//myObject.Value.GetComponent (componentName.Value).enabled = activate;
			MonoBehaviour script = (MonoBehaviour)myObject.Value.GetComponent(componentName.Value);
			script.enabled = activate;
			Finish();
		}
	}
}