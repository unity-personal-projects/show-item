﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.GameObject)]
	[Tooltip("Test if the gameobject is activated. Return a boolean.")]
	public class GameObjectIsActive : FsmStateAction
	{

		[RequiredField]
		public FsmOwnerDefault myObject;


		[ActionSection("Boolean Exit")]

		[UIHint(UIHint.Variable)]
		[Tooltip("The value of the boolean")]
		public FsmBool isActive;

		public override void Reset()
		{
			myObject = null;
			isActive = false;
		}

		public override void OnEnter()
		{
			isActive.Value = Fsm.GetOwnerDefaultTarget (myObject).activeInHierarchy;
			Finish();
		}
	}
}