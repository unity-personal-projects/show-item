﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.GUI)]
	[Tooltip("Get current button. Return next button")]
	public class TestCurrentButton : FsmStateAction
	{

		[RequiredField]
		public FsmGameObject parentMenu;
		[RequiredField]
		public FsmGameObject currentButton;


		[ActionSection("Previous and Next buttons")]

		[UIHint(UIHint.Variable)]
		[Tooltip("The value of the next button")]
		public FsmGameObject nextButton;

		[UIHint(UIHint.Variable)]
		[Tooltip("The value of the previous button.")]
		public FsmGameObject previousButton;

		public override void Reset()
		{
			currentButton = null;
			nextButton = null;
		}

		public override void OnEnter()
		{
			Transform[] buttons = parentMenu.Value.GetComponentsInChildren<Transform> ();

			if (currentButton.Value == buttons[1].gameObject) {
				nextButton.Value = buttons[3].gameObject;
				previousButton.Value = buttons[5].gameObject;
				currentButton.Value = buttons [1].gameObject;
			}
			else if (currentButton.Value == buttons[3].gameObject) {
				nextButton.Value = buttons[5].gameObject;
				previousButton.Value = buttons[1].gameObject;
				currentButton.Value = buttons [3].gameObject;
			}
			else if (currentButton.Value == buttons[5].gameObject) {
				nextButton.Value = buttons[1].gameObject;
				previousButton.Value = buttons[3].gameObject;
				currentButton.Value = buttons [5].gameObject;
			}

			Finish();
		}
	}
}