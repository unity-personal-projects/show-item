﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.StateMachine)]
	[Tooltip("Get current button. Return next button")]
	public class MySetFsmVariable : FsmStateAction
	{

		[RequiredField]
		public FsmGameObject startVariable;


		[ActionSection("setted Next button")]

		[UIHint(UIHint.Variable)]
		[Tooltip("The value of the next button")]
		public FsmGameObject endVariable;

		public override void Reset()
		{
			startVariable = null;
			endVariable = null;
		}

		public override void OnEnter()
		{
			endVariable.Value = startVariable.Value;
			Finish();
		}
	}
}